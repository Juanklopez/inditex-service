insert into PRICE(id, product_id, brand_id, currency, start_date, end_date, price, priority)
values (1, 35455, 1, 'EUR', TIMESTAMP '2020-06-14 00:00:00', TIMESTAMP '2020-12-31 23:59:59', 35.50, 0);
insert into PRICE(id, product_id, brand_id, currency, start_date, end_date, price, priority)
values (2, 35455, 1, 'EUR', TIMESTAMP '2020-06-14 15:00:00', TIMESTAMP '2020-06-14 18:30:00', 25.45, 1);
insert into PRICE(id, product_id, brand_id, currency, start_date, end_date, price, priority)
values (3, 35455, 1, 'EUR', TIMESTAMP '2020-06-15 00:00:00', TIMESTAMP '2020-06-15 11:00:00', 30.50, 1);
insert into PRICE(id, product_id, brand_id, currency, start_date, end_date, price, priority)
values (4, 35455, 1, 'EUR', TIMESTAMP '2020-06-15 16:00:00', TIMESTAMP '2020-12-31 23:59:59', 38.95, 1);