create table PRICE
(
    id         int primary key,
    product_id int        not null,
    brand_id   int        not null,
    currency   varchar(3) not null,
    start_date timestamp  not null,
    end_date   timestamp  not null,
    price      double     not null,
    priority   int        not null
);
