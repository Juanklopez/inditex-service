package com.inditex.inditexservice.db.repository;

import com.inditex.inditexservice.db.entity.PriceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRepository extends CrudRepository<PriceEntity, Integer> {

}
