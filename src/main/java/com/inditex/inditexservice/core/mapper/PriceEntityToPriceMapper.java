package com.inditex.inditexservice.core.mapper;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.db.entity.PriceEntity;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

@Mapper
public interface PriceEntityToPriceMapper extends Converter<PriceEntity, Price> {

    @Override
    Price convert(PriceEntity price);
}
