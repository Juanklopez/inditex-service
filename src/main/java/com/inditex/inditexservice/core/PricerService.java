package com.inditex.inditexservice.core;

import com.inditex.inditexservice.core.model.Price;

import java.time.LocalDateTime;

public interface PricerService {
    Price getPrice(int productId, int brandId, LocalDateTime date);
}
