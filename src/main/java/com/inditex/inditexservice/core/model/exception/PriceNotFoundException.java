package com.inditex.inditexservice.core.model.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PriceNotFoundException extends RuntimeException {

    private static final String ERROR_MESSAGE = "Price not found";

    public PriceNotFoundException() {
        super(ERROR_MESSAGE);
    }
}
