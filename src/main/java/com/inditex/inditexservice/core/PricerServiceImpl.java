package com.inditex.inditexservice.core;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.core.model.exception.PriceNotFoundException;
import com.inditex.inditexservice.db.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class PricerServiceImpl implements PricerService {

    private final PriceRepository priceRepository;
    private final ConversionService conversionService;

    @Override
    public Price getPrice(int productId, int brandId, LocalDateTime date) {
        List<Price> priceList = getPriceListFromPriceEntityList();
        return priceList.stream()
                .filter(price -> filterPrice(productId, brandId, date, price))
                .max(Comparator.comparing(Price::getPriority))
                .orElseThrow(PriceNotFoundException::new);
    }

    private boolean filterPrice(int productId, int brandId, LocalDateTime date, Price price) {
        return productId == price.getProductId()
                && brandId == price.getBrandId()
                && date.isBefore(price.getEndDate())
                && date.isAfter(price.getStartDate());
    }

    private List<Price> getPriceListFromPriceEntityList() {
        return StreamSupport.stream(priceRepository.findAll().spliterator(), true)
                .map(priceEntity -> conversionService.convert(priceEntity, Price.class))
                .collect(Collectors.toList());
    }
}
