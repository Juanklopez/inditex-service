package com.inditex.inditexservice.ws;

import com.inditex.inditexservice.core.PricerService;
import com.inditex.inditexservice.ws.model.PriceDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping(value = "/pricer")
@RequiredArgsConstructor
public class PricerController {

    private final PricerService pricerService;
    private final ConversionService conversionService;

    @GetMapping(value = "/{productId}/{brandId}/{date}")
    public ResponseEntity<PriceDTO> getPrices(
            @PathVariable int productId,
            @PathVariable int brandId,
            @PathVariable @DateTimeFormat(pattern = "yyyyMMddHHmm") LocalDateTime date) {
        log.info("Request - path=/{}/{}/{}", productId, brandId, date);

        PriceDTO priceDTO = conversionService.convert(pricerService.getPrice(productId, brandId, date), PriceDTO.class);

        log.info("Response - {}", priceDTO);
        return ResponseEntity.ok(priceDTO);
    }
}
