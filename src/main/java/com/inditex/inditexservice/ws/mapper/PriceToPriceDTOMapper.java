package com.inditex.inditexservice.ws.mapper;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.ws.model.PriceDTO;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

@Mapper
public interface PriceToPriceDTOMapper extends Converter<Price, PriceDTO> {

    @Override
    PriceDTO convert(Price price);
}
