package com.inditex.inditexservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.inditex.inditexservice")
//@EntityScan(basePackages = "com.inditex.inditexservice.db.entity")
//@EnableJpaRepositories(basePackages = "com.inditex.inditexservice.db.repository")
public class InditexServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InditexServiceApplication.class, args);
    }

}
