package com.inditex.inditexservice;

import com.inditex.inditexservice.ws.model.PriceDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InditexServiceApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void givenRequestDay14At10WhenGetPriceThenReturn3550() {
        PriceDTO priceDTO = this.restTemplate.getForObject("http://localhost:" + port + "/pricer/35455/1/202006141000",
                PriceDTO.class);
        assertNotNull(priceDTO);
        assertEquals(1, priceDTO.getBrandId());
        assertEquals(35455, priceDTO.getProductId());
        assertEquals(LocalDateTime.of(2020, 6, 14, 0, 0, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2020, 12, 31, 23, 59, 59), priceDTO.getEndDate());
        assertEquals("EUR", priceDTO.getCurrency());
        assertEquals(35.5, priceDTO.getPrice(), 0);
    }

    @Test
    public void givenRequestDay14At16WhenGetPriceThenReturn2545() throws Exception {
        PriceDTO priceDTO = this.restTemplate.getForObject("http://localhost:" + port + "/pricer/35455/1/202006141600",
                PriceDTO.class);
        assertNotNull(priceDTO);
        assertEquals(1, priceDTO.getBrandId());
        assertEquals(35455, priceDTO.getProductId());
        assertEquals(LocalDateTime.of(2020, 6, 14, 15, 0, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2020, 6, 14, 18, 30, 0), priceDTO.getEndDate());
        assertEquals("EUR", priceDTO.getCurrency());
        assertEquals(25.45, priceDTO.getPrice(), 0);
    }

    @Test
    public void givenRequestDay14At21WhenGetPriceThenReturn3550() {
        PriceDTO priceDTO = this.restTemplate.getForObject("http://localhost:" + port + "/pricer/35455/1/202006142100",
                PriceDTO.class);
        assertNotNull(priceDTO);
        assertEquals(1, priceDTO.getBrandId());
        assertEquals(35455, priceDTO.getProductId());
        assertEquals(LocalDateTime.of(2020, 6, 14, 0, 0, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2020, 12, 31, 23, 59, 59), priceDTO.getEndDate());
        assertEquals("EUR", priceDTO.getCurrency());
        assertEquals(35.5, priceDTO.getPrice(), 0);
    }

    @Test
    public void givenRequestDay15At10WhenGetPriceThenReturn3050() {
        PriceDTO priceDTO = this.restTemplate.getForObject("http://localhost:" + port + "/pricer/35455/1/202006151000",
                PriceDTO.class);
        assertNotNull(priceDTO);
        assertEquals(1, priceDTO.getBrandId());
        assertEquals(35455, priceDTO.getProductId());
        assertEquals(LocalDateTime.of(2020, 6, 15, 0, 0, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2020, 6, 15, 11, 0, 0), priceDTO.getEndDate());
        assertEquals("EUR", priceDTO.getCurrency());
        assertEquals(30.50, priceDTO.getPrice(), 0);
    }

    @Test
    public void givenRequestDay16At21WhenGetPriceThenReturn3895() {
        PriceDTO priceDTO = this.restTemplate.getForObject("http://localhost:" + port + "/pricer/35455/1/202006162100",
                PriceDTO.class);
        assertNotNull(priceDTO);
        assertEquals(1, priceDTO.getBrandId());
        assertEquals(35455, priceDTO.getProductId());
        assertEquals(LocalDateTime.of(2020, 6, 15, 16, 0, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2020, 12, 31, 23, 59, 59), priceDTO.getEndDate());
        assertEquals("EUR", priceDTO.getCurrency());
        assertEquals(38.95, priceDTO.getPrice(), 0);
    }

}
