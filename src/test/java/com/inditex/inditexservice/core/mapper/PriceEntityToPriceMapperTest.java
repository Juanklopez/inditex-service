package com.inditex.inditexservice.core.mapper;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.db.entity.PriceEntity;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PriceEntityToPriceMapperTest {

    private final PriceEntityToPriceMapper priceEntityToPriceMapper = Mappers.getMapper(PriceEntityToPriceMapper.class);

    @Test
    public void convertShouldReturnPrice() {
        PriceEntity priceEntity = new PriceEntity();
        priceEntity.setProductId(5);
        priceEntity.setBrandId(10);
        priceEntity.setStartDate(LocalDateTime.of(2020, 10, 10, 20, 0));
        priceEntity.setEndDate(LocalDateTime.of(2021, 11, 11, 21, 1));
        priceEntity.setPrice(100.50);
        priceEntity.setCurrency("EUR");

        Price price = priceEntityToPriceMapper.convert(priceEntity);

        assertNotNull(price);
        assertEquals(5, price.getProductId());
        assertEquals(10, price.getBrandId());
        assertEquals(LocalDateTime.of(2020, 10, 10, 20, 0), price.getStartDate());
        assertEquals(LocalDateTime.of(2021, 11, 11, 21, 1), price.getEndDate());
        assertEquals(100.50, price.getPrice(), 0);
        assertEquals("EUR", price.getCurrency());
    }
}