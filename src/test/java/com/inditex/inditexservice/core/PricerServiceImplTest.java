package com.inditex.inditexservice.core;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.core.model.exception.PriceNotFoundException;
import com.inditex.inditexservice.db.entity.PriceEntity;
import com.inditex.inditexservice.db.repository.PriceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PricerServiceImplTest {

    @InjectMocks
    private PricerServiceImpl pricerService;

    @Mock
    private PriceRepository priceRepository;
    @Mock
    private ConversionService conversionService;

    @Test(expected = PriceNotFoundException.class)
    public void getPriceShouldReturnPriceNotFoundExceptionWhenProductIdNotFound() {
        List<PriceEntity> priceEntityList = Collections.singletonList(new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.MIN;
        LocalDateTime endDate = LocalDateTime.MAX;
        Price price = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        when(conversionService.convert(any(), any())).thenReturn(price);

        Price priceResult = null;
        try {
            priceResult = pricerService.getPrice(50, 1, LocalDateTime.of(2020, 10, 10, 1, 1));
        } catch (PriceNotFoundException priceNotFoundException) {
            assertEquals("Price not found", priceNotFoundException.getMessage());
            throw priceNotFoundException;
        } finally {
            assertNull(priceResult);
            verify(conversionService, times(1)).convert(any(), any());
        }
    }

    @Test(expected = PriceNotFoundException.class)
    public void getPriceShouldReturnPriceNotFoundExceptionWhenBrandIdNotFound() {
        List<PriceEntity> priceEntityList = Collections.singletonList(new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.MIN;
        LocalDateTime endDate = LocalDateTime.MAX;
        Price price = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        when(conversionService.convert(any(), any())).thenReturn(price);

        Price priceResult = null;
        try {
            priceResult = pricerService.getPrice(5, 10, LocalDateTime.of(2020, 10, 10, 1, 1));
        } catch (PriceNotFoundException priceNotFoundException) {
            assertEquals("Price not found", priceNotFoundException.getMessage());
            throw priceNotFoundException;
        } finally {
            assertNull(priceResult);
            verify(conversionService, times(1)).convert(any(), any());
        }
    }

    @Test(expected = PriceNotFoundException.class)
    public void getPriceShouldReturnPriceNotFoundExceptionWhenDateBeforeStartDate() {
        List<PriceEntity> priceEntityList = Collections.singletonList(new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.of(2020, 10, 10, 1, 1, 0);
        LocalDateTime endDate = LocalDateTime.MAX;
        Price price = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        when(conversionService.convert(any(), any())).thenReturn(price);

        Price priceResult = null;
        try {
            priceResult = pricerService.getPrice(5, 1, LocalDateTime.of(2020, 10, 10, 1, 0, 59));
        } catch (PriceNotFoundException priceNotFoundException) {
            assertEquals("Price not found", priceNotFoundException.getMessage());
            throw priceNotFoundException;
        } finally {
            assertNull(priceResult);
            verify(conversionService, times(1)).convert(any(), any());
        }
    }

    @Test(expected = PriceNotFoundException.class)
    public void getPriceShouldReturnPriceNotFoundExceptionWhenDateAfterEndDate() {
        List<PriceEntity> priceEntityList = Collections.singletonList(new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.MIN;
        LocalDateTime endDate = LocalDateTime.of(2020, 10, 10, 1, 1, 0);
        Price price = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        when(conversionService.convert(any(), any())).thenReturn(price);

        Price priceResult = null;
        try {
            priceResult = pricerService.getPrice(5, 1, LocalDateTime.of(2020, 10, 10, 1, 1, 1));
        } catch (PriceNotFoundException priceNotFoundException) {
            assertEquals("Price not found", priceNotFoundException.getMessage());
            throw priceNotFoundException;
        } finally {
            assertNull(priceResult);
            verify(conversionService, times(1)).convert(any(), any());
        }
    }

    @Test
    public void getPriceShouldReturnPriceWithValidFilters() {
        List<PriceEntity> priceEntityList = List.of(new PriceEntity(), new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.MIN;
        LocalDateTime endDate = LocalDateTime.MAX;
        Price price1 = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        Price price2 = Price.builder().brandId(2).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        when(conversionService.convert(any(), any()))
                .thenReturn(price1, price2);

        Price priceResult = pricerService.getPrice(5, 2, LocalDateTime.of(2020, 10, 10, 1, 1));

        assertNotNull(priceResult);
        assertEquals(2, priceResult.getBrandId());
        assertEquals(5, priceResult.getProductId());
        assertEquals(startDate, priceResult.getStartDate());
        assertEquals(endDate, priceResult.getEndDate());
        assertEquals("EUR", priceResult.getCurrency());
        assertEquals(0, priceResult.getPriority());
        assertEquals(12.5, priceResult.getPrice(), 0);

        verify(conversionService, times(2)).convert(any(), any());
    }

    @Test
    public void getPriceShouldReturnPriceWithBiggerPriority() {
        List<PriceEntity> priceEntityList = List.of(new PriceEntity(), new PriceEntity(), new PriceEntity());
        //when(priceRepository.findAll()).thenReturn(priceEntityList);

        LocalDateTime startDate = LocalDateTime.MIN;
        LocalDateTime endDate = LocalDateTime.MAX;
        Price price1 = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(12.5).priority(0).build();
        Price price2 = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(32.5).priority(1).build();
        Price price3 = Price.builder().brandId(1).productId(5).startDate(startDate).endDate(endDate).currency("EUR").price(37.7).priority(2).build();
        when(conversionService.convert(any(), any()))
                .thenReturn(price1, price2, price3);

        Price priceResult = pricerService.getPrice(5, 1, LocalDateTime.of(2020, 10, 10, 1, 1));

        assertNotNull(priceResult);
        assertEquals(1, priceResult.getBrandId());
        assertEquals(5, priceResult.getProductId());
        assertEquals(startDate, priceResult.getStartDate());
        assertEquals(endDate, priceResult.getEndDate());
        assertEquals("EUR", priceResult.getCurrency());
        assertEquals(2, priceResult.getPriority());
        assertEquals(37.7, priceResult.getPrice(), 0);

        verify(conversionService, times(3)).convert(any(), any());
    }
}