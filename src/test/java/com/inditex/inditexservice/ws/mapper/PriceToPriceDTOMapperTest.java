package com.inditex.inditexservice.ws.mapper;

import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.ws.model.PriceDTO;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PriceToPriceDTOMapperTest {

    private final PriceToPriceDTOMapper priceToPriceDTOMapper = Mappers.getMapper(PriceToPriceDTOMapper.class);

    @Test
    public void convertShouldReturnPriceDTO() {
        Price price = Price.builder()
                .productId(5)
                .brandId(10)
                .startDate(LocalDateTime.of(2020, 10, 10, 20, 0))
                .endDate(LocalDateTime.of(2021, 11, 11, 21, 1))
                .price(100.50)
                .currency("EUR")
                .priority(1)
                .build();

        PriceDTO priceDTO = priceToPriceDTOMapper.convert(price);

        assertNotNull(priceDTO);
        assertEquals(5, priceDTO.getProductId());
        assertEquals(10, priceDTO.getBrandId());
        assertEquals(LocalDateTime.of(2020, 10, 10, 20, 0), priceDTO.getStartDate());
        assertEquals(LocalDateTime.of(2021, 11, 11, 21, 1), priceDTO.getEndDate());
        assertEquals(100.50, priceDTO.getPrice(), 0);
        assertEquals("EUR", priceDTO.getCurrency());
    }
}