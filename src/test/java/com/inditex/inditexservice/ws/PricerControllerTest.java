package com.inditex.inditexservice.ws;

import com.inditex.inditexservice.core.PricerService;
import com.inditex.inditexservice.core.model.Price;
import com.inditex.inditexservice.core.model.exception.PriceNotFoundException;
import com.inditex.inditexservice.ws.model.PriceDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(PricerController.class)
public class PricerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private PricerService pricerService;
    @Mock
    private ConversionService conversionService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new PricerController(pricerService, conversionService))
                .alwaysExpect(forwardedUrl(null))
                .build();
    }

    @Test
    public void getPriceShouldReturnOkStatusWithPriceDTO() throws Exception {
        Price price = Price.builder().build();
        when(pricerService.getPrice(1, 2, LocalDateTime.of(2021, 5, 30, 11, 0))).thenReturn(price);

        PriceDTO priceDTO = PriceDTO.builder()
                .productId(1)
                .brandId(2)
                .startDate(LocalDateTime.of(2021, 5, 31, 11, 0))
                .endDate(LocalDateTime.of(2021, 6, 1, 11, 0))
                .price(0.50)
                .currency("EUR")
                .build();
        when(conversionService.convert(price, PriceDTO.class)).thenReturn(priceDTO);

        this.mockMvc.perform(get("/pricer/1/2/202105301100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId", is(1)))
                .andExpect(jsonPath("$.brandId", is(2)))
                .andExpect(jsonPath("$.startDate[0]", is(2021)))
                .andExpect(jsonPath("$.startDate[1]", is(5)))
                .andExpect(jsonPath("$.startDate[2]", is(31)))
                .andExpect(jsonPath("$.startDate[3]", is(11)))
                .andExpect(jsonPath("$.startDate[4]", is(0)))
                .andExpect(jsonPath("$.endDate[0]", is(2021)))
                .andExpect(jsonPath("$.endDate[1]", is(6)))
                .andExpect(jsonPath("$.endDate[2]", is(1)))
                .andExpect(jsonPath("$.endDate[3]", is(11)))
                .andExpect(jsonPath("$.endDate[4]", is(0)))
                .andExpect(jsonPath("$.price").value(is(0.50), Double.class))
                .andExpect(jsonPath("$.currency", is("EUR")));

        verify(pricerService, times(1)).getPrice(anyInt(), anyInt(), any(LocalDateTime.class));
        verify(conversionService, times(1)).convert(any(), any());
    }

    @Test
    public void getPriceShouldReturnBadRequestWhenException() throws Exception {
        doThrow(new PriceNotFoundException()).when(pricerService).getPrice(1, 2, LocalDateTime.of(2021, 5, 30, 11, 0));

        this.mockMvc.perform(get("/pricer/1/2/202105301100"))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(pricerService, times(1)).getPrice(anyInt(), anyInt(), any(LocalDateTime.class));
        verify(conversionService, times(0)).convert(any(), any());
    }
}